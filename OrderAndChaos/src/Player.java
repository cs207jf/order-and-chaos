
public class Player {

   private Game game;
   
   private boolean isHuman;
   
   private String playerName;
   
   public Game getGame() {
      return this.game;
   }
   
   public String getPlayerName() {
      return this.playerName;
   }
   
   public boolean isIsHuman() {
      return this.isHuman;
   }
   
   public void setGame(Game value) {
      this.game = value;
   }
   
   public void setIsHuman(boolean value) {
      this.isHuman = value;
   }
   
   public void setPlayerName(String value) {
      this.playerName = value;
   }
   
   public Player(String pName){
	
	   playerName = pName;
   }
   
   }

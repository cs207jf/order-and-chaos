package with_GUI__TEST;
public class Game {

	public class Acheivement {
		String name;
		int value;
	}

	private Board board = new Board();
	public final static int[] token = { 0, 1, -1 };
	private int turnCounter;
	private Player[] players = new Player[2];
	private boolean aI;
	private boolean humanChaos;

	/*
	 * Increments turn counter for use later in current player
	 */
	public void incTurnCounter() {
		turnCounter++;
	}

	/*
	 * Calculates current player based on whether the turn number is even
	 * 
	 * @return current player
	 */
	public Player currentPlayer() {
		return players[(turnCounter % 2)];
	}
	
	/*
	 * Temporary currentPlayerInt() for gui testing
	 */
	public int currentPlayerInt(){
		return turnCounter % 2;
	}

	/*
	 * Calls setSquare on given token and coordinates
	 * 
	 * @param token, coordinates
	 * 
	 * @return true if move was successful, else false
	 */
	public boolean makeMove(int token, int x, int y) {
		if (board.setSquare(token, x, y,currentPlayer())) {
			System.out.println("Move was Successful\n");
			incTurnCounter();
			board.detectWin(Board.getSquare(x, y));
			return true;
		}
		System.out.println("Square has already been taken");
		return false;
	}
	
	/*
	 * Returns the gameboard for displaying
	 */
	public Square[][] getBoard(){
		return board.getBoard();
	}

	public void gameSetup(boolean aI, boolean humanTypeChaos) {
		this.aI = aI;
		this.humanChaos = humanTypeChaos;
	}
	
	/*
	 * Save selected objects to the file save.ser
	 * 
	 * throws exception if file not found.
	 */
//	public void serialize(){
//		
//		try{
//			FileOutputStream fs = new FileOutputStream("save.ser");
//			ObjectOutputStream os = new ObjectOutputStream(fs);
//			os.writeObject(instance);
//			os.close();
//		}
//		catch(Exception ex){
//			
//			ex.printStackTrace();
//		}
//		
//	}
	
	/*
	 * Opens the file save.ser and reads contents of required objects
	 * 
	 * throws exception if file not found.
	 */
//	public void deserialize(){
//		
//		try{
//			FileInputStream fs = new FileInputStream("save.ser");
//			ObjectInputStream os = new ObjectInputStream(fs);
//			Object one = os.readObject();
//			instance = (Game) one;
//			os.close();
//		}
//		catch(Exception ex){
//			
//			ex.printStackTrace();
//		}
//	}

}
















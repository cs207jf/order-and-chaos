package with_GUI__TEST;
import java.util.Scanner;

public class test {
	private static Square[][] board;
	private static Game session;

	public static void main(String[] args) {
		session = new Game();
		@SuppressWarnings("resource")
		Scanner userInput = new Scanner(System.in);
		System.out.println("Welcome \nFor help type @help\nHow would you like to play: single / multi");
		boolean AI = userInput.nextLine().toLowerCase().contains("single");
		if (AI) {
			System.out.println("Would you like to be: order / chaos");
			boolean humanTypeChaos = userInput.nextLine().toLowerCase().contains("chaos");
			session.gameSetup(AI,humanTypeChaos);
		}
		
		boolean play = true;
		while (play) {
			printTurn();
			String input = userInput.nextLine();
			if (input.contains("@")) {
				if (input.contains("help")) {
					printHelp();
				}
				if (input.contains("turn")) {
					printTurn();
				}
				if (input.contains("print")) {
					printBoard();
				}
				if (input.contains("quit")) {
					quit();
				}
				if (input.contains("test")) {
					testCall(input.charAt(5));
				}
				if (input.contains("undo")) {
					// Undo Goes Here
				}
			} else if (input.charAt(0) == 'o' || input.charAt(0) == 'x') {
				makeMove(input);
			} else {
				System.out.println("Invalid Input!\nType @help for help");
			}
		}
	}

	private static void makeMove(String input) {
		if (input.length() == 3) {
			int token = input.charAt(0) == 'x' ? 1 : -1;
			int x = Character.getNumericValue(input.charAt(1));
			int y = Character.getNumericValue(input.charAt(2));
			if (x > 5 || x < 0 || y > 5 || x < 0) {
				System.out
						.println("Invalid Input. (Coordinates should be between 0-5)");
			} else {
				session.makeMove(token, x, y);
			}
			printBoard();
		} else {
			System.out.println("Input Invalid. (Type @help for help)");
		}
	}

	public static void printBoard() {
		board = session.getBoard();
		System.out.printf("  0   1   2   3   4   5\n");
		for (int i = 0; i < 6; i++) { // column
			String row = i + "";
			for (int j = 0; j < 6; j++) { // row
				String square;
				if (board[j][i].getValue() == 1) {
					square = "[x]";
				} else if (board[j][i].getValue() == -1) {
					square = "[o]";
				} else {
					square = "[ ]";
				}
				row += square + " ";
			}
			System.out.println(row);
		}
	}

	private static void printHelp() {
		System.out
				.println("--- Help ---\n@turn - Display's whose turn it is\n@print - displays  the board\n@quit - ends the program");

	}

	private static void printTurn() {
		System.out.println((session.currentPlayer().isTypeChaos() ? "Chaos" : "Order")
				+ " its your turn");

	}

	private static void quit() {
		System.out.println("Cheerio");
		System.exit(0);
	}

	private static void testCall(char c) {
		if (c == '1') {
			System.out.println("Negative Diagonal Test");
			System.out.println("x00");
			makeMove("x00");
			System.out.println("x11");
			makeMove("x11");
			System.out.println("x22");
			makeMove("x22");
			System.out.println("x33");
			makeMove("x33");
			System.out.println("x44");
			makeMove("x44");
		}
		if (c == '2') {
			System.out.println("Positive Diagonal Test");
			System.out.println("x50");
			makeMove("x50");
			System.out.println("x41");
			makeMove("x41");
			System.out.println("x32");
			makeMove("x32");
			System.out.println("x23");
			makeMove("x23");
			System.out.println("x14");
			makeMove("x14");
			System.out.println("x05");
			makeMove("x05");
		}
		if (c == '3') {
			System.out.println("Column Diagonal Test");
			System.out.println("x50");
			makeMove("x50");
			System.out.println("x40");
			makeMove("x40");
			System.out.println("x30");
			makeMove("x30");
			System.out.println("x20");
			makeMove("x20");
			System.out.println("x10");
			makeMove("x10");
			System.out.println("x00");
			makeMove("x00");
		}
		if (c == '4') {
			System.out.println("Negative Row Diagonal Test");
			System.out.println("x05");
			makeMove("x05");
			System.out.println("x04");
			makeMove("x04");
			System.out.println("x03");
			makeMove("x03");
			System.out.println("x02");
			makeMove("x02");
			System.out.println("x01");
			makeMove("x01");
			System.out.println("x00");
			makeMove("x00");
		}
		if (c == '5') {
			makeMove("x00");
			makeMove("x01");
			makeMove("o02");
			makeMove("o03");
			makeMove("x04");
			makeMove("x05");
			makeMove("o10");
			makeMove("o11");
			makeMove("x12");
			makeMove("x13");
			makeMove("o14");
			makeMove("o15");
			makeMove("x20");
			makeMove("x21");
			makeMove("o22");
			makeMove("o23");
			makeMove("x24");
			makeMove("x25");
			makeMove("o30");
			makeMove("o31");
			makeMove("x32");
			makeMove("x33");
			makeMove("o34");
			makeMove("o35");
			makeMove("x40");
			makeMove("x41");
			makeMove("o42");
			makeMove("o43");
			makeMove("x44");
			makeMove("x45");
			makeMove("o50");
			makeMove("o51");
			makeMove("x52");
			makeMove("x53");
			makeMove("o54");
			makeMove("o55");
		}
	}
}

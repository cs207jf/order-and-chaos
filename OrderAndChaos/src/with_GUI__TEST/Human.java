package with_GUI__TEST;

public class Human extends Player {
  
   private String playerID;
   
   public String getPlayerID() {
      return this.playerID;
   }
   
   public void setPlayerID(String value) {
      this.playerID = value;
   }
   
   public Human(boolean typeC){
	   super(typeC);
   }
   
   }

package with_GUI__TEST;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class GUI extends JFrame implements ActionListener, FocusListener {

	private static Square[][] board;
	private static JTextField[][] grid;
	private static Game session;
	private static JTextArea txtpnConsole;

	public static void consoleOut(String string) {
		System.out.println("# "+ string);
		txtpnConsole.append("\n" +string);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				session = new Game();
				printBoard();
			}
		});
	}

	private static void makeMove() {
		String token = tokenComboBox.getSelectedItem().toString();
		int x = (int) xSpinner.getValue();
		int y = (int) ySpinner.getValue();
		if (token.contains("T")) {
			testCall(x);
		} else {
			int tokValue = token.contains("X") ? 1 : -1;
			if(session.makeMove(tokValue, x, y)){
				consoleOut("Move "+token + x+ y+ " was successful");
			}
			else{
				consoleOut("Square already taken. \n\tTry Again");
			}
		}
		printBoard();
		printTurn();
	}
	
	private static void makeMove(String testString){
		int tokValue = testString.charAt(0) == 'x' ? 1 : -1;
		int x = Character.getNumericValue(testString.charAt(1));
		int y = Character.getNumericValue(testString.charAt(2));
		if(session.makeMove(tokValue, x, y)){
			consoleOut("Test Move "+testString+ " was successful");
		}
		else{
			consoleOut("Square already taken. ");
		}
		printBoard();
	}

	public static void printBoard() {
		board = session.getBoard();
		for (int i = 0; i < 6; i++) { // column
			for (int j = 0; j < 6; j++) { // row
				String square;
				if (board[j][i].getValue() == 1) {
					square = "x";
				} else if (board[j][i].getValue() == -1) {
					square = "o";
				} else {
					square = " ";
				}
				grid[i][j].setText(square);
			}
		}
	}


	private static void printTurn() {
		consoleOut("\n"+(session.currentPlayerInt()==0? "Order" : "Chaos")
				+ " its your turn");

	}

	private static void quit() {
		consoleOut("Cheerio");
		System.exit(0);
	}

	private JPanel contentPane;
	private static JComboBox<String> tokenComboBox;
	private static JSpinner xSpinner;
	private static JSpinner ySpinner;

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel gridAreaPanel = new JPanel();
		contentPane.add(gridAreaPanel, BorderLayout.CENTER);
		gridAreaPanel.setLayout(new GridLayout(6, 6, 1, 1));

		grid = new JTextField[6][6];
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				grid[i][j] = new JTextField();
				grid[i][j].setName(j+""+i);
				grid[i][j].setEditable(false);
				grid[i][j].addFocusListener(this);
				gridAreaPanel.add(grid[i][j]);
				grid[i][j].setColumns(1);
			}
		}

		JPanel makeMovePanel = new JPanel();
		contentPane.add(makeMovePanel, BorderLayout.SOUTH);
		makeMovePanel.setLayout(new BorderLayout(0, 0));

		JButton btnEnter = new JButton("Enter");
		btnEnter.addActionListener(this);
		makeMovePanel.add(btnEnter, BorderLayout.EAST);

		JPanel makeMoveInputPanel = new JPanel();
		makeMovePanel.add(makeMoveInputPanel, BorderLayout.CENTER);
		makeMoveInputPanel.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel tokenInputPanel = new JPanel();
		makeMoveInputPanel.add(tokenInputPanel);

		JLabel lblToken = new JLabel("Token");
		tokenInputPanel.add(lblToken);

		tokenComboBox = new JComboBox<String>();
		tokenComboBox.setModel(new DefaultComboBoxModel<String>(new String[] { "X",
				"O", "T" }));
		tokenInputPanel.add(tokenComboBox);

		JPanel coordinateInputPanel = new JPanel();
		makeMoveInputPanel.add(coordinateInputPanel);
		coordinateInputPanel.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel xInputPanel = new JPanel();
		coordinateInputPanel.add(xInputPanel);

		JLabel lblX = new JLabel("X");
		xInputPanel.add(lblX);

		xSpinner = new JSpinner(new SpinnerNumberModel(0,0,5,1));
		xInputPanel.add(xSpinner);

		JPanel yInputPanel = new JPanel();
		coordinateInputPanel.add(yInputPanel);

		JLabel lblY = new JLabel("Y");
		yInputPanel.add(lblY);

		ySpinner = new JSpinner(new SpinnerNumberModel(0,0,5,1));
		yInputPanel.add(ySpinner);

		JPanel sidebarPanel = new JPanel();
		contentPane.add(sidebarPanel, BorderLayout.EAST);
		sidebarPanel.setLayout(new BorderLayout(0, 0));

		JPanel controlButtonsPanels = new JPanel();
		sidebarPanel.add(controlButtonsPanels, BorderLayout.SOUTH);
		controlButtonsPanels.setLayout(new GridLayout(0, 2, 0, 0));

		JButton btnTurn = new JButton("Turn");
		btnTurn.addActionListener(this);
		controlButtonsPanels.add(btnTurn);

		JButton btnQuit = new JButton("Quit");
		btnQuit.addActionListener(this);
		controlButtonsPanels.add(btnQuit);

		JPanel consolePanel = new JPanel();
		sidebarPanel.add(consolePanel);
		consolePanel.setLayout(new BoxLayout(consolePanel, BoxLayout.X_AXIS));

		txtpnConsole = new JTextArea();
		txtpnConsole.setColumns(20);
		txtpnConsole.setLineWrap(true);
		txtpnConsole.setEditable(false);
		txtpnConsole.setCaretPosition(txtpnConsole.getDocument().getLength());
		consoleOut("Console");
        JScrollPane jp = new JScrollPane(txtpnConsole);
        consolePanel.add(jp);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if (command.equals("Enter")) {
            makeMove();
        }
		else if(command.equals("Quit")){
			quit();
		}
		else if(command.equals("Turn")){
			printTurn();
		}
	}

	@Override
	public void focusGained(FocusEvent e) {
		String square = e.getComponent().getName();
		System.out.println("\nFocus on square " +square);
		char x = square.charAt(0);
		char y = square.charAt(1);
		xSpinner.setValue(Character.getNumericValue(x));
		ySpinner.setValue(Character.getNumericValue(y));
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private static void testCall(int c) {
		if (c == 1) {
			consoleOut("Negative Diagonal Test");
			makeMove("x00");
			makeMove("x11");
			makeMove("x22");
			makeMove("x33");
			makeMove("x44");
		}
		if (c == 2) {
			consoleOut("Positive Diagonal Test");
			makeMove("x50");
			makeMove("x41");
			makeMove("x32");
			makeMove("x23");
			makeMove("x14");
			makeMove("x05");
		}
		if (c == 3) {
			consoleOut("Column Diagonal Test");
			makeMove("x50");
			makeMove("x40");
			makeMove("x30");
			makeMove("x20");
			makeMove("x10");
			makeMove("x00");
		}
		if (c == 4) {
			consoleOut("Negative Row Diagonal Test");
			makeMove("x05");
			makeMove("x04");
			makeMove("x03");
			makeMove("x02");
			makeMove("x01");
			makeMove("x00");
		}
		if (c == 5) {
			makeMove("x00");
			makeMove("x01");
			makeMove("o02");
			makeMove("o03");
			makeMove("x04");
			makeMove("x05");
			makeMove("o10");
			makeMove("o11");
			makeMove("x12");
			makeMove("x13");
			makeMove("o14");
			makeMove("o15");
			makeMove("x20");
			makeMove("x21");
			makeMove("o22");
			makeMove("o23");
			makeMove("x24");
			makeMove("x25");
			makeMove("o30");
			makeMove("o31");
			makeMove("x32");
			makeMove("x33");
			makeMove("o34");
			makeMove("o35");
			makeMove("x40");
			makeMove("x41");
			makeMove("o42");
			makeMove("o43");
			makeMove("x44");
			makeMove("x45");
			makeMove("o50");
			makeMove("o51");
			makeMove("x52");
			makeMove("x53");
			makeMove("o54");
			makeMove("o55");
		}
	}

}

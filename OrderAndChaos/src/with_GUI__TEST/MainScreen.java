package with_GUI__TEST;
import java.util.Scanner;

public class MainScreen {
	public void printBoard(Board board) {
		System.out.println("Current Board");
		for (int i = 0; i < board.getBoard().length; i++) {
			System.out.println(board.getBoard()[i] + "\n");
		}
	}

	public String[] setupGame() {
		String gameMode, player1Name, player2Name;
		@SuppressWarnings("resource")
		Scanner scanIn = new Scanner(System.in);
		System.out.println("Please enter your username:");
		player1Name = scanIn.next();
		System.out
				.println("For Human vs Human type- H \t For Human vs AI type- A\n What type of game would you like?");
		gameMode = scanIn.next().toLowerCase();
		if (gameMode.contains("h")) {
			System.out.println("Player 2 please enter your username:");
			player2Name = scanIn.next();
		} else {
			player2Name = "Err";
		}
		System.out.println("Starting Game");
		String[] settings = { gameMode, player1Name, player2Name };
		return settings;
	}

	public void turn(int turn) {
		System.out.println("Player " + turn + " its your turn");
	}

	public void win(int player) {
		System.out.println("Player " + player + "you win!");
	}
}

package with_AI;

import java.io.Serializable;

public class Board implements Serializable {
	private Square[][] board;
	int countRow = 0;
	int countCol = 0;
	int countNegDiag = 0;
	int countPosDiag = 0;

	public Board() {
		setBoard();
	}

	public boolean setSquare(int i, int x, int y, Player player) {
		return board[x][y].setValue(i,player);
	}
	
	public void unSetSquare(int x, int y){
		board[x][y].unSetValue();
	}
	
	public Square getSquare(int x, int y) {
		return board[x][y];
	}

	
	public Square[][] getBoard() {
		return board;
	}
	public void setBoard(){
		board = new Square[6][6];
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				Square newSquare = new Square();
				newSquare.setValue(0);
				board[i][j] = newSquare;
				board[i][j].setX(i);
				board[i][j].setY(j);
			}
		}
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				initLinks(i,j);
			}
		}
	}

	public int searchLeft(Square currentSquare) {
		if (currentSquare.getLeft() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getLeft();
			if (current.getValue() == next.getValue()) {
				countRow++; 
				searchLeft(next);
			}
		}
		return 0;
	}
	public int searchRight(Square currentSquare) {
		if (currentSquare.getRight() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getRight();
			if (current.getValue() == next.getValue()) {
				countRow++; 
				searchRight(next);
			}
		}
		return 0;
	}
	public int searchUp(Square currentSquare) {
		if (currentSquare.getUp() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getUp();
			if (current.getValue() == next.getValue()) {
				countCol++;
				searchUp(next);
			}
		}
		return 0;
	}
	public int searchDown(Square currentSquare) {
		if (currentSquare.getDown() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getDown();
			if (current.getValue() == next.getValue()) {
				countCol++;
				searchDown(next);
			}
		}
		return 0;
	}
	public int searchUpLeft(Square currentSquare) {
		if (currentSquare.getUpLeft() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getUpLeft();
			if (current.getValue() == next.getValue()) {
				countNegDiag++;
				searchUpLeft(next);
			}
		}
		return 0;
	}
	public int searchDownRight(Square currentSquare) {
		if (currentSquare.getDownRight() != null) {

			Square current = currentSquare;
			Square next = currentSquare.getDownRight();

			if (current.getValue() == next.getValue()) {
				countNegDiag++;
				searchDownRight(next);
			}
		}
		return 0;
	}
	public int searchUpRight(Square currentSquare) {
		if (currentSquare.getUpRight() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getUpRight();
			if (current.getValue() == next.getValue()) {
				countPosDiag++;
				searchUpRight(next);
			}
		}
		return 0;
	}
	public int searchDownLeft(Square currentSquare) {
		if (currentSquare.getDownLeft() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getDownLeft();
			if (current.getValue() == next.getValue()) {
				countPosDiag++; 
				searchDownLeft(next);
			}
		}
		return 0;
	}
	
	public boolean detectWin(Square lastPlayed) {
		// Row Search
		searchLeft(lastPlayed);
		searchRight(lastPlayed);
		if (countRow > 3) 
		{
			System.out.println("A Winning Row was detected");
			System.out.println("Winner : Order");
			return true;
		}

		// Column Search
		searchUp(lastPlayed);
		searchDown(lastPlayed);
		if (countCol > 3) 
		{	System.out.println("A Winning Column was detected");
			System.out.println("Winner : Order");
			return true;
		}

		// Diagonal (Positive) Search
		searchDownLeft(lastPlayed);
		searchUpRight(lastPlayed);
		if (countPosDiag >3) 
		{
			System.out.println("A Winning Diagonal (Positive) was detected");
			System.out.println("Winner : Order");
			return true;
		}

		// Diagonal (Negative) Search
		searchUpLeft(lastPlayed);
		searchDownRight(lastPlayed);
		if (countNegDiag >3) 
		{
			System.out.println("A Winning Diagonal (Negative) was detected");
			System.out.println("Winner : Order");
			return true;
		}
		if(BoardFull()){
			System.out.println("Board is full, no more moves possible");
			System.out.println("Winner : Chaos");
			return true;
		}
		//Reset Counters 
		countPosDiag = 0;
		countNegDiag = 0;
		countCol =0;
		countRow = 0;
		return false;
	}
	
	
	public boolean BoardFull() {
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				if(!board[i][j].isFilled()){
					return false;
				}
			}
		}
		return true;
	}
	
	public void initLinks(int x,int y) {
		if (x == 0) {
			board[x][y].setLeft(null);
			board[x][y].setUpLeft(null);
			board[x][y].setDownLeft(null);
		} else {
			board[x][y].setLeft(getSquare(x - 1, y));
			if (y != 5) {
				board[x][y].setUpLeft(getSquare(x - 1, y + 1));
			}
			if (y != 0) {
				board[x][y].setDownLeft(getSquare(x - 1, y - 1));
			}
		}
		if (x == 5) {
			board[x][y].setRight(null);
			board[x][y].setUpRight(null);
			board[x][y].setDownRight(null);
		} else {
			board[x][y].setRight(getSquare(x + 1, y));
			if (y != 5) {
				board[x][y].setUpRight(getSquare(x + 1, y + 1));
			}
			if (y != 0) {
				board[x][y].setDownRight(getSquare(x + 1, y - 1));
			}
		}
		if (y == 0) {
			board[x][y].setDown(null);
			board[x][y].setDownLeft(null);
			board[x][y].setDownRight(null);
		} else {
			board[x][y].setDown(getSquare(x, y - 1));
			if (x != 0) {
				board[x][y].setDownLeft(getSquare(x - 1, y - 1));
			}
			if (x != 5) {
				board[x][y].setDownRight(getSquare(x + 1, y - 1));
			}
		}

		if (y == 5) {
			board[x][y].setUp(null);
			board[x][y].setUpLeft(null);
			board[x][y].setUpRight(null);
		} else {
			board[x][y].setUp(getSquare(x, y + 1));
			if (x != 0) {
				board[x][y].setUpLeft(getSquare(x - 1, y + 1));
			}
			if (x != 5) {
				board[x][y].setUpRight(getSquare(x + 1, y + 1));
			}
		}
	}
	public int acheivementDetection(){
		if(smileyAcheivement()){
			return 1;
		}
		return 0;
	}
	
	public boolean smileyAcheivement(){
		boolean eyeL = board[1][1].isFilled();
		boolean eyeR = board[3][1].isFilled();
		boolean mouth1 = board[0][3].isFilled();
		boolean mouth2 = board[1][4].isFilled();
		boolean mouth3 = board[2][4].isFilled();
		boolean mouth4 = board[3][4].isFilled();
		boolean mouth5 = board[4][3].isFilled();
		return(eyeL&&eyeR&&mouth1&&mouth2&&mouth3&&mouth4&&mouth5);
	}
}

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.SwingConstants;


public class TestMainScreen extends JFrame {

	private JPanel contentPane;
	private JTextField txtGameHasStarted;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestMainScreen frame = new TestMainScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestMainScreen() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 599, 406);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnGame = new JMenu("Game");
		menuBar.add(mnGame);
		
		JMenuItem mntmNewGame = new JMenuItem("New Game");
		mnGame.add(mntmNewGame);
		
		JMenuItem mntmSaveGame = new JMenuItem("Save Game");
		mnGame.add(mntmSaveGame);
		
		JMenuItem mntmQuitGame = new JMenuItem("Quit Game");
		mnGame.add(mntmQuitGame);
		
		JMenu mnMove = new JMenu("Move");
		menuBar.add(mnMove);
		
		JMenuItem mntmUndo = new JMenuItem("Undo");
		mnMove.add(mntmUndo);
		
		JMenuItem mntmRedo = new JMenuItem("Redo");
		mnMove.add(mntmRedo);
		
		JMenu mnStatistics = new JMenu("Statistics");
		menuBar.add(mnStatistics);
		
		JMenuItem mntmStatistiscs = new JMenuItem("Statistics");
		mnStatistics.add(mntmStatistiscs);
		
		JMenuItem mntmAcheivements = new JMenuItem("Acheivements");
		mnStatistics.add(mntmAcheivements);
		
		JMenuItem mntmLeaderboard = new JMenuItem("Leaderboard");
		mnStatistics.add(mntmLeaderboard);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mnHelp.add(mntmHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.EAST);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Token", null, panel, null);
		panel.setLayout(new GridLayout(0, 1, 2, 0));
		
		JButton btnO = new JButton("O");
		panel.add(btnO);
		
		JButton btnX = new JButton("X");
		panel.add(btnX);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Statistiscs", null, panel_2, null);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JTextPane txtpnYouMadeA = new JTextPane();
		txtpnYouMadeA.setText("You Made A Tetris Shape\nYou Covered over 30% of the Board");
		panel_2.add(txtpnYouMadeA);
		
		txtGameHasStarted = new JTextField();
		txtGameHasStarted.setHorizontalAlignment(SwingConstants.LEFT);
		txtGameHasStarted.setEditable(false);
		txtGameHasStarted.setBackground(Color.WHITE);
		txtGameHasStarted.setText("Game has started...");
		tabbedPane.addTab("Console", null, txtGameHasStarted, null);
		txtGameHasStarted.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(245, 245, 220));
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(6, 6, 0, 0));
		
		JButton btnA = new JButton("A1");
		panel_1.add(btnA);
		
		JButton btnA_1 = new JButton("A2");
		panel_1.add(btnA_1);
		
		JButton btnA_2 = new JButton("A3");
		panel_1.add(btnA_2);
		
		JButton btnA_3 = new JButton("A4");
		panel_1.add(btnA_3);
		
		JButton btnA_4 = new JButton("A5");
		panel_1.add(btnA_4);
		
		JButton btnA_5 = new JButton("A6");
		panel_1.add(btnA_5);
		
		JButton btnB = new JButton("B1");
		panel_1.add(btnB);
		
		JButton btnB_1 = new JButton("B2");
		panel_1.add(btnB_1);
		
		JButton btnB_2 = new JButton("B3");
		panel_1.add(btnB_2);
		
		JButton btnB_3 = new JButton("B4");
		panel_1.add(btnB_3);
		
		JButton btnB_4 = new JButton("B5");
		panel_1.add(btnB_4);
		
		JButton btnB_5 = new JButton("B6");
		panel_1.add(btnB_5);
		
		JButton btnC = new JButton("C1");
		panel_1.add(btnC);
		
		JButton btnC_1 = new JButton("C2");
		panel_1.add(btnC_1);
		
		JButton btnC_2 = new JButton("C3");
		panel_1.add(btnC_2);
		
		JButton btnC_3 = new JButton("C4");
		panel_1.add(btnC_3);
		
		JButton btnC_4 = new JButton("C5");
		panel_1.add(btnC_4);
		
		JButton btnC_5 = new JButton("C6");
		panel_1.add(btnC_5);
		
		JButton btnD = new JButton("D1");
		panel_1.add(btnD);
		
		JButton btnD_1 = new JButton("D2");
		panel_1.add(btnD_1);
		
		JButton btnD_2 = new JButton("D3");
		panel_1.add(btnD_2);
		
		JButton btnD_3 = new JButton("D4");
		panel_1.add(btnD_3);
		
		JButton btnD_4 = new JButton("D5");
		panel_1.add(btnD_4);
		
		JButton btnD_5 = new JButton("D6");
		panel_1.add(btnD_5);
		
		JButton btnE = new JButton("E1");
		panel_1.add(btnE);
		
		JButton btnE_1 = new JButton("E2");
		panel_1.add(btnE_1);
		
		JButton btnE_2 = new JButton("E3");
		panel_1.add(btnE_2);
		
		JButton btnE_3 = new JButton("E4");
		panel_1.add(btnE_3);
		
		JButton btnE_4 = new JButton("E5");
		panel_1.add(btnE_4);
		
		JButton btnE_5 = new JButton("E6");
		panel_1.add(btnE_5);
		
		JButton btnF = new JButton("F1");
		panel_1.add(btnF);
		
		JButton btnF_1 = new JButton("F2");
		panel_1.add(btnF_1);
		
		JButton btnF_2 = new JButton("F3");
		panel_1.add(btnF_2);
		
		JButton btnF_3 = new JButton("F4");
		panel_1.add(btnF_3);
		
		JButton btnF_4 = new JButton("F5");
		panel_1.add(btnF_4);
		
		JButton btnF_5 = new JButton("F6");
		panel_1.add(btnF_5);
	}

}

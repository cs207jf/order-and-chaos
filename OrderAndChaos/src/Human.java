
public class Human extends Player {
  
   private String playerID;
   
   public String getPlayerID() {
      return this.playerID;
   }
   
   public void setPlayerID(String value) {
      this.playerID = value;
   }
   
   public Human(String pName, String pid){
	   
	   super(pName);
	   playerID = pid;
   }
   
   }

public class Board {
	private static Square[][] board;

	public Board() {
		board = new Square[6][6];
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				Square newSquare = new Square(i, j);
				newSquare.setValue(0);
				board[i][j] = newSquare;
			}
		}
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 6; j++) {
				board[i][j].initLinks();
			}
		}
	}

	/*
	 * Changes the value of a square to match the token return false if the
	 * square is already taken.
	 * 
	 * @param token value, coordinates of square in question
	 */
	public boolean setSquare(int i, int x, int y, int player) {
		return board[x][y].setValue(i,player);
	}

	/*
	 * Returns the whole board for print purposes
	 */
	public Square[][] getBoard() {
		return board;
	}

	/*
	 * Return the token value of the square
	 * 
	 * @param coordinates of Grid Square in question
	 * 
	 * @return token value or 0 for empty square
	 */
	public static Square getSquare(int x, int y) {
		return board[x][y];
	}

	/*
	 * Return the number of consecutive squares to the left of current square
	 * with the same token
	 */
	int countRow = 0;

	public int searchLeft(Square currentSquare) {
		if (currentSquare.getLeft() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getLeft();
			if (current.getValue() == next.getValue()) {
				countRow++; 
				searchLeft(next);
			}
		}
		return 0;
	}
	
	public int searchRight(Square currentSquare) {
		if (currentSquare.getRight() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getRight();
			if (current.getValue() == next.getValue()) {
				countRow++; 
				searchRight(next);
			}
		}
		return 0;
	}

	/*
	 * Return the number of consecutive squares to the up of current square with
	 * the same token
	 */
	int countCol = 0;

	public int searchUp(Square currentSquare) {
		if (currentSquare.getUp() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getUp();
			if (current.getValue() == next.getValue()) {
				countCol++;
				searchUp(next);
			}
		}
		return 0;
	}

	public int searchDown(Square currentSquare) {
		if (currentSquare.getDown() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getDown();
			if (current.getValue() == next.getValue()) {
				countCol++;
				searchDown(next);
			}
		}
		return 0;
	}

	/*
	 * Return the number of consecutive squares to the upLeft of current square
	 * with the same token
	 */
	int countNegDiag = 0;

	public int searchUpLeft(Square currentSquare) {
		if (currentSquare.getUpLeft() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getUpLeft();
			if (current.getValue() == next.getValue()) {
				countNegDiag++;
				searchUpLeft(next);
			}
		}
		return 0;
	}
	public int searchDownRight(Square currentSquare) {
		if (currentSquare.getDownRight() != null) {

			Square current = currentSquare;
			Square next = currentSquare.getDownRight();

			if (current.getValue() == next.getValue()) {
				countNegDiag++;
				searchDownRight(next);
			}
		}
		return 0;
	}

	/*
	 * Return the number of consecutive squares to the upRight of current square
	 * with the same token
	 */
	int countPosDiag = 0;

	public int searchUpRight(Square currentSquare) {
		if (currentSquare.getUpRight() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getUpRight();
			if (current.getValue() == next.getValue()) {
				countPosDiag++;
				searchUpRight(next);
			}
		}
		return 0;
	}

	public int searchDownLeft(Square currentSquare) {
		if (currentSquare.getDownLeft() != null) {
			Square current = currentSquare;
			Square next = currentSquare.getDownLeft();
			if (current.getValue() == next.getValue()) {
				countPosDiag++; 
				searchDownLeft(next);
			}
		}
		return 0;
	}

	

	/*
	 * Will call the appropriate chain check method depending on grid position
	 * 
	 * @param coordinates of grid square in question
	 * 
	 * @return true if winning chain is detected
	 */
	public boolean detectWin(Square lastPlayed) {
		// Row Search
		searchLeft(lastPlayed);
		searchRight(lastPlayed);
		if (countRow > 3) 
		{
			System.out.println("A Winning Row was detected");
			System.out.println("Winner : Order");
			return true;
		}

		// Column Search
		searchUp(lastPlayed);
		searchDown(lastPlayed);
		if (countCol > 3) 
		{	System.out.println("A Winning Column was detected");
			System.out.println("Winner : Order");
			return true;
		}

		// Diagonal (Positive) Search
		searchDownLeft(lastPlayed);
		searchUpRight(lastPlayed);
		if (countPosDiag >3) 
		{
			System.out.println("A Winning Diagonal (Positive) was detected");
			System.out.println("Winner : Order");
			return true;
		}

		// Diagonal (Negative) Search
		searchUpLeft(lastPlayed);
		searchDownRight(lastPlayed);
		if (countNegDiag >3) 
		{
			System.out.println("A Winning Diagonal (Negative) was detected");
			System.out.println("Winner : Order");
			return true;
		}
		if(BoardFull()){
			System.out.println("Board is full, no more moves possible");
			System.out.println("Winner : Chaos");
			return true;
		}
		//Reset Counters 
		countPosDiag = 0;
		countNegDiag = 0;
		countCol =0;
		countRow = 0;
		return false;
	}

	private boolean BoardFull() {
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				if(!board[i][j].isFilled()){
					return false;
				}
			}
		}
		return true;
	}
}

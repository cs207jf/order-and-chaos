package GUIAdded;
import java.awt.GridLayout;

import javax.swing.JDialog;
import javax.swing.JTextArea;


public class AcheivementGUI extends JDialog {

	public  JTextArea txtrAcheivements = new JTextArea();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			AcheivementGUI dialog = new AcheivementGUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AcheivementGUI() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setAlwaysOnTop(true);
		setTitle("Acheivements");
		setResizable(false);
		setBounds(100, 100, 450, 262);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		{
			txtrAcheivements.setEditable(false);
			txtrAcheivements.setText("No Acheivements So Far");
			txtrAcheivements.setRows(15);
			getContentPane().add(txtrAcheivements);
		}
	}

}

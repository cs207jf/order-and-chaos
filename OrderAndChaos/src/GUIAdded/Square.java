package GUIAdded;


import java.io.Serializable;

public class Square implements Serializable{
	private int value;
	private Square left;
	private Square right;
	private Square up;
	private Square down;
	private Square upLeft;
	private Square upRight;
	private Square downLeft;
	private Square downRight;
	private boolean filled;
	private Player player;
	private int x;
	private int y;

	public Square() {
		filled = false;
	}

	

	public int getValue() {
		return value;
	}

	public void unSetValue(){
		filled = false;
		value = 0;
		player = null;
	}
	public boolean setValue(int value) {
		if (filled) {
			return false;
		} else {
			this.value = value;
			return true;
		}
	}

	public boolean setValue(int value, Player player2) {
		if (filled) {
			return false;
		} else {
			this.value = value;
			setFilled();
			this.player = player2;
			return true;
		}
	}

	public Square getLeft() {
		return left;
	}

	public void setLeft(Square left) {
		this.left = left;
	}

	public Square getRight() {
		return right;
	}

	public void setRight(Square right) {
		this.right = right;
	}

	public Square getUp() {
		return up;
	}

	public void setUp(Square up) {
		this.up = up;
	}

	public Square getDown() {
		return down;
	}

	public void setDown(Square down) {
		this.down = down;
	}

	public Square getUpLeft() {
		return upLeft;
	}

	public void setUpLeft(Square upLeft) {
		this.upLeft = upLeft;
	}

	public Square getUpRight() {
		return upRight;
	}

	public void setUpRight(Square upRight) {
		this.upRight = upRight;
	}

	public Square getDownLeft() {
		return downLeft;
	}

	public void setDownLeft(Square downLeft) {
		this.downLeft = downLeft;
	}

	public Square getDownRight() {
		return downRight;
	}

	public void setDownRight(Square downRight) {
		this.downRight = downRight;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled() {
		this.filled = true;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}



	public int getX() {
		return x;
	}



	public void setX(int x) {
		this.x = x;
	}



	public int getY() {
		return y;
	}



	public void setY(int y) {
		this.y = y;
	}
}

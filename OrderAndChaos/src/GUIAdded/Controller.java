package GUIAdded;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

public class Controller implements Serializable{
	private static Game session;
	
	private static boolean AI;
	private static GameSetupGUI GameSetupScreen = new GameSetupGUI();
	private static LoadSavedGameGUI LoadGameScreen = new LoadSavedGameGUI();
	private static MainScreenGUI MainPlayScreen = new MainScreenGUI();
	private static MakeMoveGUI MakeMoveScreen = new MakeMoveGUI();
	private static AcheivementGUI AcheivementScreen = new AcheivementGUI();
	private static boolean deserialised;
	private static ArrayList<Acheivement> oldAcheivements = new ArrayList<Acheivement>();
	public static boolean GameWon;

	
	public static void main(String[] args){
		deserialised =true;
		GameWon =false;
		LoadGame();
		oldAcheivements = session.acheivements;
		startMainPlayScreen();
		if(deserialised){
			startLoadGameScreen();
		}
		else{
		startGameSetupScreen();
		session.acheivements = oldAcheivements;
		}
	}

	public static void NewGame() {
		startGameSetupScreen();
		System.out.println("Refreshing Board");
		refreshBoard();
	}

	public static void RestartGame() {
		session.resetBoard();
		session.resetTurnCounter();
		refreshBoard();
	}
	
	public static void QuitGame(){
		int quit = confirmDialog("Are you sure you want to quit?");
		if(quit ==0){
			SaveGame();
		System.exit(0);
		}
	}

	public static void StartMove() {
		// TODO Auto-generated method stub
		startMakeMoveScreen();
	}
	
	public static void MakeMove(String token, int x, int y) {
		if(GameWon){
			alert("You can't make anymore moves. The game has already been won");
		}
		else{
		int tok = token.contains("x")? -1:1;
		session.makeMove(tok, x, y);
		endMakeMoveScreen();
		startMainPlayScreen();
		refreshBoard();
		AIMoveDetector();
		}
	}
		
	public static void UndoMove(){
		if(GameWon){
			alert("You can't undo any moves. The game has been won");
		}
		else{
		if (AI) {
		session.undoMove();
		refreshBoard();
		}
		else{
			alert("You can only undo when playing against AI");
		}
		}
		
	}


	public static void ViewAcheivements() {
		AcheivementScreen.setVisible(true);
		if(session.acheivements.size()>0){
			AcheivementScreen.txtrAcheivements.setText("Acheivements\n");
		}
		for (int j = 0; j < session.acheivements.size(); j++) {
			AcheivementScreen.txtrAcheivements.append(session.acheivements.get(j).name + ": " +session.acheivements.get(j).description +"\n");
			}
	}

	public static void Help() {
		// TODO Auto-generated method stub
		
	}

	public static void startGameSetupScreen(){ 
		GameSetupScreen.setVisible(true); 
		log("Game Setup Window: VISIBLE");
		}
	public static void endGameSetupScreen(){ 
		GameSetupScreen.setVisible(false); 
		log("Game Setup Window: HIDDEN");
		}
	
	public static void startLoadGameScreen(){ 
		LoadGameScreen.setVisible(true);
		log("Load Game Window: VISIBLE");
		}
	public static void endLoadGameScreen(){ 
		LoadGameScreen.setVisible(false); 
		log("Load Game Window: HIDDEN");
		}
	
	public static void startMainPlayScreen(){ 
		MainPlayScreen.setVisible(true); 
		log("Main Play Window: VISIBLE");
		}
	public static void endMainPlayScreen(){ 
		MainPlayScreen.setVisible(false); 
		log("Main Play window: HIDDEN");
		}
	
	public static void startMakeMoveScreen(){ 
		MakeMoveScreen.setVisible(true); 
		log("Make Move window: VISIBLE");
		}
	public static void endMakeMoveScreen(){ 
		MakeMoveScreen.setVisible(false); 
		log("Make Move window: HIDDEN");
		}
	
	public static void refreshBoard(){
		MainPlayScreen.printBoard(session.getBoard());
	}

	private static void AIMoveDetector() {
		if(!GameWon){
		if(!session.getCurrentPlayer().isHuman()){
			log("Its AI's Turn");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int[] AIMoveVals = session.makeAIMove();
			alert("AI made its move \n X: "+AIMoveVals[1]+" Y: "+AIMoveVals[2]);
		}
		}
		
	}

	public static void SetupGame(String[] args) {
		AI = args[0].contains("Computer");
		boolean HumanType = args[1].contains("Chaos");
		session = new Game(AI,HumanType);
		log("New game was setup with AI:" + AI + " and Player as "+(HumanType? "Chaos":"Order"));
		endGameSetupScreen();
		refreshBoard();
		AIMoveDetector();
	}
	
	public static void log(String event){
		Date date = new Date();
		try {
			FileWriter f = new FileWriter("log.txt",true);
			BufferedWriter bw = new BufferedWriter(f);
			bw.write(date.toString() + ": ");
			bw.write(event);
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void alert(String message){
		JOptionPane.showMessageDialog(null, message);
	}
	
	public static int confirmDialog(String message){
		return JOptionPane.showConfirmDialog(null,message);
	}

	public static void LoadGame() {
		try {
			FileInputStream fs = new FileInputStream("save.ser");
			ObjectInputStream os = new ObjectInputStream(fs);
			Object one = os.readObject();
			//Object two = os.readObject();
			session = (Game) one;
			//ac = (Acheivement) two;
			fs.close();
		} catch (IOException i) {
			i.printStackTrace();
			deserialised = false;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			deserialised =false;
		}
		refreshBoard();
	}

	public static void SaveGame() {
		try {
			FileOutputStream fs = new FileOutputStream("save.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(session);
			//os.writeObject(ac);
			os.close();
			fs.close();
		} catch (IOException ex) {

			ex.printStackTrace();
		}
		alert("Game was saved");
	}
}

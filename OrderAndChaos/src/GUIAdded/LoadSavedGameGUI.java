package GUIAdded;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class LoadSavedGameGUI extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private boolean decision = false;
	private static boolean loadGame = false;

	/**
	 * Create the dialog.
	 */
	public LoadSavedGameGUI() {
		setTitle("Order And Chaos");
		setBounds(100, 100, 268, 126);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		{
			JLabel lblASavedGame = new JLabel("A Saved Game was detected!");
			contentPanel.add(lblASavedGame);
		}
		{
			JLabel lblWouldYouLike = new JLabel("Would you like to continue playing?");
			contentPanel.add(lblWouldYouLike);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Yes");
				okButton.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent arg0) {
						Controller.endLoadGameScreen();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("No");
				cancelButton.addMouseListener(new MouseAdapter(){
					public void mouseClicked(MouseEvent arg0){
						Controller.NewGame();
						Controller.endLoadGameScreen();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}

package GUIAdded;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MakeMoveGUI extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private static JComboBox<String> Xval = new JComboBox<String>();
	private static JComboBox<String> Yval = new JComboBox<String>();
	private static JComboBox<String> Tval = new JComboBox<String>();

	public MakeMoveGUI() {
		setTitle("Make Move");
		setBounds(100, 100, 361, 142);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			JLabel lblSelectWhichToken = new JLabel(
					"Select which token you would like to play.");
			contentPanel.add(lblSelectWhichToken);
		}
		{
			Tval.setModel(new DefaultComboBoxModel<String>(new String[] { "X",
					"O" }));
			contentPanel.add(Tval);
		}
		{
			JLabel lblSelectYourCoordinates = new JLabel(
					"Select your co-ordinates");
			contentPanel.add(lblSelectYourCoordinates);
		}
		{
			JLabel lblX = new JLabel("X:");
			contentPanel.add(lblX);
		}
		{
			Xval.setModel(new DefaultComboBoxModel<String>(new String[] { "0",
					"1", "2", "3", "4", "5" }));
			contentPanel.add(Xval);
		}
		{
			JLabel lblY = new JLabel("Y:");
			contentPanel.add(lblY);
		}
		{
			Yval.setModel(new DefaultComboBoxModel<String>(new String[] { "0",
					"1", "2", "3", "4", "5" }));
			contentPanel.add(Yval);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						int x = Integer.parseInt(Xval.getSelectedItem()
								.toString());
						int y = Integer.parseInt(Yval.getSelectedItem()
								.toString());
						String t = Tval.getSelectedItem().toString();
						Controller.MakeMove(t, x, y);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Controller.endMakeMoveScreen();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}

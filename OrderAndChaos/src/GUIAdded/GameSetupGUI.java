package GUIAdded;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;

import java.awt.GridLayout;

import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GameSetupGUI extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JComboBox<String> gameType = new JComboBox<String>();
	private JComboBox<String> playerType = new JComboBox<String>();

	/**
	 * Create the dialog.
	 */
	public GameSetupGUI() {
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 488, 166);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 2, 0, 0));
		{
			JLabel lblGameType = new JLabel(
					"Who would you like to play against?");
			contentPanel.add(lblGameType);
		}
		{
			gameType.setModel(new DefaultComboBoxModel<String>(new String[] {
					"Computer", "Player" }));
			contentPanel.add(gameType);
		}
		{
			JLabel lblPlayerType = new JLabel(
					"What player would you like to be?");
			contentPanel.add(lblPlayerType);
		}
		{
			playerType.setModel(new DefaultComboBoxModel<String>(new String[] {
					"Order", "Chaos" }));
			contentPanel.add(playerType);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Start Game");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String gameT = gameType.getSelectedItem().toString();
						String playerT = playerType.getSelectedItem()
								.toString();
						String[] args = { gameT, playerT };
						Controller.SetupGame(args);
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton btnShowAcheivements = new JButton("Show Acheivements");
				btnShowAcheivements.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Controller.ViewAcheivements();
					}
				});
				buttonPane.add(btnShowAcheivements);
			}
			{
				JButton cancelButton = new JButton("Quit");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Controller.QuitGame();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

}

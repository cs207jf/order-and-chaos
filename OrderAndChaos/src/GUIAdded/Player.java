package GUIAdded;


import java.io.Serializable;

public class Player implements Serializable{

	private boolean isOrder;
	private boolean isHuman;

	public Player(boolean human, boolean type) {
		this.isHuman = human;
		this.isOrder = type;

	}

	public boolean isHuman() {
		return isHuman;
	}

	public boolean isOrder() {
		return isOrder;
	}

}

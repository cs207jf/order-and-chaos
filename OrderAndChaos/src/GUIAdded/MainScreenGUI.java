package GUIAdded;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;


public class MainScreenGUI extends JFrame {

	private JPanel contentPane;
	private JPanel[] gridSquares = new JPanel[36];
	
	public MainScreenGUI() {
		setTitle("Order And Chaos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 400);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnGame = new JMenu("Game");
		menuBar.add(mnGame);
		
		JMenuItem mntmStartANew = new JMenuItem("New Game");
		mntmStartANew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Controller.NewGame();
			}
		});
		mnGame.add(mntmStartANew);
		
		JMenuItem mntmRestartCurrentGame = new JMenuItem("Restart");
		mntmRestartCurrentGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Controller.RestartGame();
			}
		});
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Controller.SaveGame();
			}
		});
		mnGame.add(mntmSave);
		
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Controller.LoadGame();
			}
		});
		mnGame.add(mntmLoad);
		mnGame.add(mntmRestartCurrentGame);
		
		JMenuItem mntmQuit = new JMenuItem("Quit ");
		mntmQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Controller.QuitGame();
			}
		});
		mnGame.add(mntmQuit);
		
		JMenu mnMove = new JMenu("Move");
		menuBar.add(mnMove);
		
		JMenuItem mntmMakeAMove = new JMenuItem("Make A Move");
		mnMove.add(mntmMakeAMove);
		mntmMakeAMove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Controller.StartMove();
			}
		});
		
		JMenuItem mntmUndoMove = new JMenuItem("Undo Move");
		mnMove.add(mntmUndoMove);
		mntmUndoMove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Controller.UndoMove();
			}
		});
		
		JMenu mnInformation = new JMenu("Information");
		menuBar.add(mnInformation);
		
		JMenuItem mntmViewAcheivments = new JMenuItem("View Acheivments");
		mnInformation.add(mntmViewAcheivments);
		mntmViewAcheivments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Controller.ViewAcheivements();
			}
		});
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmQuickStart = new JMenuItem("View Log");
		mntmQuickStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File log = new File("log.txt");
				try {
					java.awt.Desktop.getDesktop().edit(log);
				} catch (IOException e1) {
					Controller.log("File Error - Log File");
					Controller.alert("File Seems to be missing sorry!");
				}
				catch (IllegalArgumentException e2){
					Controller.log("File Error - Log File");
					Controller.alert("File Seems to be missing sorry!");
				}
			}
		});
		mnHelp.add(mntmQuickStart);
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mnHelp.add(mntmHelp);
		mntmHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File help = new File("help.txt");
				try {
					java.awt.Desktop.getDesktop().edit(help);
				} catch (IOException e1) {
					Controller.log("File Error - Help File");
					Controller.alert("File Seems to be missing sorry!");
				}
				catch (IllegalArgumentException e2){
					Controller.log("File Error - Log File");
					Controller.alert("File Seems to be missing sorry!");
				}
			}
		});
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				Controller.startMakeMoveScreen();
			}
		});
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(6, 0, 0, 0));
		
		for (int i =0; i<36;i++){
			gridSquares[i] = new JPanel();
			gridSquares[i].setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
			panel.add(gridSquares[i] );
			JLabel label = new JLabel(" ");
			gridSquares[i].add(label);
		}
	}
	
	public void printBoard(Board board){
		int k =0;
		for(int i=0;i<6;i++){
			for(int j=0;j<6;j++){
				int valueInt = board.getSquare(i,j).getValue();
				String valueChar;
				if(valueInt ==1){
					valueChar = "X";
				}
				else if(valueInt == -1){
					valueChar = "O";
				}
				else{
					valueChar = "";
				}
				((JLabel) gridSquares[k].getComponent(0)).setText(valueChar);
				k++;
			}
		}
	}

}

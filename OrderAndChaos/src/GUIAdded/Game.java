package GUIAdded;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;


public class Game implements Serializable {
	private Board board;
	private int turnCounter;
	private Player[] players;
	private Move[] previousMoves;
	private boolean undoPossible;
	public ArrayList<Acheivement> acheivements = new ArrayList<Acheivement>();

	public Game(boolean ai, boolean aiChaos) {
		board = new Board();
		players = new Player[2];
		setPlayers(ai, aiChaos);
		previousMoves = new Move[2];
		Acheivement newGame = new Acheivement(1,"Welcome","You played your first game");
		Boolean newGameAcheived = false;
		for(int i=0; i<acheivements.size(); i++){
			if(acheivements.get(i).id == 1){
				newGameAcheived =true;
			}
		}
		if(!newGameAcheived){
		acheivements.add(newGame);
		}
	}

	public void resetTurnCounter(){
		turnCounter = 0;
	}
	public void incTurnCounter() {
		turnCounter++;
	}

	public int getTurnCounter() {
		return turnCounter;
	}

	public Player getCurrentPlayer() {
		return players[(turnCounter % 2)];
	}

	public Player getPlayer(int playerNo) {
		return players[playerNo];
	}

	public void setPlayers(boolean ai, boolean aiOrder) {
		if (ai) {
			if (aiOrder) {
				players[0] = new Player(false, true);
				players[1] = new Player(true, false);
			} else {
				players[0] = new Player(true, true);
				players[1] = new Player(false, false);
				;
			}
		} else {
			players[0] = new Player(true, true);
			players[1] = new Player(true, false);
		}
	}

	public Board getBoard() {
		return board;
	}

	public void resetBoard() {
		board.setBoard();
	}

	public boolean makeMove(int token, int x, int y) {
		if (setSquare(x, y, token)) {
			incTurnCounter();
			previousMoves[0] = previousMoves[1];
			Move thisMove = new Move(token, x, y);
			previousMoves[1] = thisMove;
			undoPossible = true;
			System.out.println("Move played:" + (token == 1 ? "[x]" : "[o]")
					+ " " + x + " " + y);
			Controller.refreshBoard();
			if (board.detectWin(getSquare(x, y))) {
				detectEndGameAcheivements();
				Controller.GameWon =true;
				Controller.alert("We have a winner!\n"+( getCurrentPlayer().isOrder()? "Order":"Chaos") + "you've won");
				//test.quit();
			}
			return true;
		}
		Controller.alert("This Square has already been taken");
		Controller.log("square has been taken");
		return false;
	}

	public void detectAchievements() {
		shapeDetection();
	}

	public void detectEndGameAcheivements() {
		// For EndGame achievements
		int ptype = (getCurrentPlayer().isOrder())? 1 : 2;
		int turnCount = getTurnCounter()/2;
		System.out.println("Turn Count: "+ turnCount);
		if (ptype == 2) {
			if(turnCount<10){
				processNewAcheivement(105);
				return;
			}
			else if(turnCount<15){
				processNewAcheivement(110);
				return;
			}
			else if(turnCount <20){
				processNewAcheivement(115);
				return;
			} 
		}
		else {
			processNewAcheivement(100);
		}
	}

	public void processNewAcheivement(int i) {
		/*
		 * 1 Smile Shape , 2 Cross Shape , 3 Circle Shape 
		 * 105 Order Win in 5 , 110 Order Win in 10, 115 Order Win in 15 
		 * 205 Chaos Win in 5 , 210 Chaos Win in 10, 215 Chaos Win in 15
		 */
		for (int j = 0; j < acheivements.size(); j++) {
			if (acheivements.get(j).id == i) {
				return;
			}
		}
		Acheivement newAcheiv = new Acheivement();
		switch (i) {
			case 1: {
				newAcheiv.id = 1;
				newAcheiv.name = "Smiley Face Shape";
				newAcheiv.description = "You recreated the smiley face \n(11)(31)((03)(14)(24)(34)(43))";
				break;
			}
			case 2: {
				newAcheiv.id = 2;
				newAcheiv.name = "Cross Shape";
				newAcheiv.description = "You recreated the cross shape \n((11)(22)(33)(44))((14)(23)(32)(41))";
				break;
			}
			case 3: {
				newAcheiv.id = 3;
				newAcheiv.name = "Circle Shape";
				newAcheiv.description = "You recreated the circle shape \n((11)(21)(31)(41))((12)(13))((14)(24)(34)(44))((42)(43))";
				break;
			}
			case 105:{
				newAcheiv.id = 105;
				newAcheiv.name = "Order Wins in >10 Moves";
				newAcheiv.description= "Order bet Chaos in less than 10 Moves";
				break;
			}
			case 110:{
				newAcheiv.id = 110;
				newAcheiv.name = "Order Wins in >15 Moves";
				newAcheiv.description= "Order bet Chaos in less than 15 Moves";
				break;
			}

			case 115:{
				newAcheiv.id = 115;
				newAcheiv.name = "Order Wins in >20 Moves";
				newAcheiv.description= "Order bet Chaos in less than 20 Moves";
				break;
			}
			case 100:{
				newAcheiv.id = 100;
				newAcheiv.name = "Chaos Wins";
				newAcheiv.description= "Chaos bet Order";
				break;}
			default: {
				return;
			}
		}
		acheivements.add(newAcheiv);
		Controller.log("\n!!!A NEW ACHEIVEMENT WAS UNLOCKED!!!\n"+newAcheiv.name+"\n");

	}

	public void undoMove() {
		if (undoPossible && getTurnCounter() > 2) {
			for (int i = 0; i < 2; i++) {
				int x = previousMoves[i].x;
				int y = previousMoves[i].y;
				board.unSetSquare(x, y);
				Controller.log("The " + ((i == 0) ? "last" : "last last")
						+ "move was undone");
			}
			undoPossible = false;
		} else {
			Controller.log("Can't Undo. You are only allow to do this once per turn");
		}
	}

	public Move random() {
		// create a random row and col number
		// between 0-5
		Random rRow = new Random();
		int row = rRow.nextInt(5 - 0) + 0;

		Random rCol = new Random();
		int col = rCol.nextInt(5 - 0) + 0;

		Random rTok = new Random();
		int tok = rTok.nextInt(1 - -1) + -1;
		if (tok == 0) {
			tok = 1;
		}
		return new Move(tok, col, row);
	}

	public int[] makeAIMove() {
		Boolean moveMade = false;
		int tok = 0;
		int row = 0;
		int col = 0;
		Move thisMove;
		while (!moveMade) {
			if (getCurrentPlayer().isOrder()) {
				thisMove = previousMoves[0];
				if (getTurnCounter() == 0) {
					Move move = random();
					tok = move.token;
					row = move.y;
					col = move.x;
					moveMade = makeMove(tok, row, col);
					int [] moveVals = {tok,row,col};
					return moveVals;
				}
				tok = thisMove.token;
			} else {
				thisMove = previousMoves[1];
				tok = thisMove.token == 1 ? -1 : 1;
			}
			// generate values in order way
			row = thisMove.x;
			col = thisMove.y;

			// has to do the opposite of down so this makes it move upwords
			Square lastSquare = getSquare(row, col);
			Square up = lastSquare.getUp();
			Square upRight = lastSquare.getUpRight();
			Square right = lastSquare.getRight();
			Square downRight = lastSquare.getDownRight();
			Square down = lastSquare.getDown();
			Square downLeft = lastSquare.getDownLeft();
			Square left = lastSquare.getLeft();
			Square upLeft = lastSquare.getUpLeft();

			// 3 Dimension View
			if (right != null && !right.isFilled() && left != null
					&& left.getLeft() != null) {
				if (lastSquare.getValue() == left.getValue()
						&& left.getValue() == left.getLeft().getValue()) {
					if (makeMove(tok, right.getX(), right.getY())) {
						Controller.log("AI USED 3D Right Rule");
						int [] moveVals = {tok,right.getX(),left.getY()};
						return moveVals;
					}
				}
			}
			if (up != null && !up.isFilled() && down != null
					&& down.getDown() != null) {
				if (lastSquare.getValue() == down.getValue()
						&& down.getValue() == down.getDown().getValue()) {
					if (makeMove(tok, up.getX(), up.getY())) {
						Controller.log("AI USED 3D Up Rule");
						int [] moveVals = {tok,up.getX(), up.getY()};
						return moveVals;
					}

				}

			}
			if (left != null && !left.isFilled() && right != null
					&& right.getRight() != null) {
				if (lastSquare.getValue() == right.getValue()
						&& right.getValue() == right.getRight().getValue()) {
					if (makeMove(tok, left.getX(), left.getY())) {
						Controller.log("AI USED 3D Left Rule");
						int [] moveVals = {tok,left.getX(), left.getY()};
						return moveVals;
					}

				}
			}
			if (upLeft != null && !upLeft.isFilled() && downRight != null
					&& downRight.getDownRight() != null) {
				if (lastSquare.getValue() == downRight.getValue()
						&& downRight.getValue() == downRight.getDownRight()
								.getValue()) {
					if (makeMove(tok, upLeft.getX(), upLeft.getY())) {
						Controller.log("AI USED 3D Up Left Rule");
						int [] moveVals = {tok, upLeft.getX(), upLeft.getY()};
						return moveVals;
					}

				}
			}
			if (downLeft != null && !downLeft.isFilled() && upRight != null
					&& downLeft.getDownLeft() != null) {
				if (lastSquare.getValue() == upRight.getValue()
						&& downLeft.getValue() == downLeft.getDownLeft()
								.getValue()) {
					if (makeMove(tok, downLeft.getX(), downLeft.getY())) {
						System.out.println("AI USED 3D Down Left Rule");
						int [] moveVals = {tok, downLeft.getX(), downLeft.getY()};
						return moveVals;
					}
				}
			}
			if (downRight != null && !downRight.isFilled() && upLeft != null
					&& downRight.getDownRight() != null) {
				if (lastSquare.getValue() == upLeft.getValue()
						&& downRight.getValue() == downRight.getDownRight()
								.getValue()) {
					if (makeMove(tok, downRight.getX(), downRight.getY())) {
						Controller.log("AI USED 3D Down Right Rule");
						int [] moveVals = {tok, downRight.getX(), downRight.getY()};
						return moveVals;
					}
				}

			}
			if (upRight != null && !upRight.isFilled() && downLeft != null
					&& downLeft.getDownLeft() != null) {
				if (lastSquare.getValue() == downLeft.getValue()
						&& downLeft.getValue() == downLeft.getDownLeft()
								.getValue()) {
					if (makeMove(tok, upRight.getX(), upRight.getY())) {
						Controller.log("AI USED 3D Up Right Rule");
						int [] moveVals = {tok, upRight.getX(), upRight.getY()};
						return moveVals;
					}
				}
			}
			if (down != null && !down.isFilled() && up != null
					&& up.getUp() != null) {
				if (lastSquare.getValue() == up.getValue()
						&& up.getValue() == up.getUp().getValue()) {
					if (makeMove(tok, down.getX(), down.getY())) {
						Controller.log("AI USED 3D Up Left Rule");
						int [] moveVals = {tok,  down.getX(), down.getY()};
						return moveVals;
					}

				}
			}

			// 2 Dimension View
			if (right != null && !right.isFilled() && left != null) {
				if (lastSquare.getValue() == left.getValue()) {
					if (makeMove(tok, right.getX(), right.getY())) {
						Controller.log("AI USED 2D Right Rule");
						int [] moveVals = {tok, right.getX(), right.getY()};
						return moveVals;
					}
				}
			}
			if (up != null && !up.isFilled() && down != null) {
				if (lastSquare.getValue() == down.getValue()) {
					if (makeMove(tok, up.getX(), up.getY())) {
						Controller.log("AI USED 2D Up Rule");
						int [] moveVals = {tok, up.getX(), up.getY()};
						return moveVals;
					}

				}

			}
			if (left != null && !left.isFilled() && right != null) {
				if (lastSquare.getValue() == right.getValue()) {
					if (makeMove(tok, left.getX(), left.getY())) {
						Controller.log("AI USED 2D Left Rule");
						int [] moveVals = {tok,left.getX(), left.getY()};
								return moveVals;
					}

				}
			}
			if (upLeft != null && !upLeft.isFilled() && downRight != null) {
				if (lastSquare.getValue() == downRight.getValue()) {
					if (makeMove(tok, upLeft.getX(), upLeft.getY())) {
						Controller.log("AI USED 2D Up Left Rule");
						int [] moveVals = {tok,upLeft.getX(), upLeft.getY()};
								return moveVals;
					}

				}
			}
			if (downLeft != null && !downLeft.isFilled() && upRight != null) {
				if (lastSquare.getValue() == upRight.getValue()) {
					if (makeMove(tok, downLeft.getX(), downLeft.getY())) {
						Controller.log("AI USED 2D Down Left Rule");
						int [] moveVals = {tok,downLeft.getX(), downLeft.getY()};
								return moveVals;
					}
				}
			}
			if (downRight != null && !downRight.isFilled() && upLeft != null) {
				if (lastSquare.getValue() == upLeft.getValue()) {
					if (makeMove(tok, downRight.getX(), downRight.getY())) {
						Controller.log("AI USED 2D Down Right Rule");
						int [] moveVals = {tok,downRight.getX(), downRight.getY()};
								return moveVals;
					}
				}

			}
			if (upRight != null && !upRight.isFilled() && downLeft != null) {
				if (lastSquare.getValue() == downLeft.getValue()) {
					if (makeMove(tok, upRight.getX(), upRight.getY())) {
						Controller.log("AI USED 2D Up Right Rule");
						int [] moveVals = {tok,upRight.getX(), upRight.getY()};
								return moveVals;
					}
				}
			}
			if (down != null && !down.isFilled() && up != null) {
				if (lastSquare.getValue() == up.getValue()) {
					if (makeMove(tok, down.getX(), down.getY())) {
						Controller.log("AI USED 2D Up Left Rule");
						int [] moveVals = {tok,down.getX(), down.getY()};
								return moveVals;
					}

				}
			}

			// 1 Dimension View
			if (right != null && !right.isFilled()) {
				if (makeMove(tok, right.getX(), right.getY())) {
					int [] moveVals = {tok,right.getX(), right.getY()};
							return moveVals;
				}
			}
			if (up != null && !up.isFilled()) {
				if (makeMove(tok, up.getX(), up.getY())) {
					int [] moveVals = {tok,up.getX(), up.getY()};
							return moveVals;
				}

			}
			if (left != null && !left.isFilled()) {
				if (makeMove(tok, left.getX(), left.getY())) {
					int [] moveVals = {tok,left.getX(), left.getY()};
							return moveVals;
				}
			}
			if (upLeft != null && !upLeft.isFilled()) {
				if (makeMove(tok, upLeft.getX(), upLeft.getY())) {
					int [] moveVals = {tok,upLeft.getX(), upLeft.getY()};
							return moveVals;
				}
			}
			if (downLeft != null && !downLeft.isFilled()) {
				if (makeMove(tok, downLeft.getX(), downLeft.getY())) {
					int [] moveVals = {tok,downLeft.getX(), downLeft.getY()};
							return moveVals;
				}
			}
			if (downRight != null && !downRight.isFilled()) {
				if (makeMove(tok, downRight.getX(), downRight.getY())) {
					int [] moveVals = {tok,downRight.getX(), downRight.getY()};
							return moveVals;
				}
			}
			if (upRight != null && !upRight.isFilled()) {
				if (makeMove(tok, upRight.getX(), upRight.getY())) {
					int [] moveVals = {tok,upRight.getX(), upRight.getY()};
							return moveVals;
				}
			}
			if (down != null && !down.isFilled()) {
				if (makeMove(tok, down.getX(), down.getY())) {
					int [] moveVals = {tok, down.getX(), down.getY()};
							return moveVals;
				}
			}

			Move move = random();
			System.out.println("AI USED Random");
			tok = move.token;
			row = move.y;
			col = move.x;
			moveMade = makeMove(tok, row, col);
		}
		int [] moveVals = {tok, row, col};
		return moveVals;

	}

	public Move[] getMoves() {
		return previousMoves;
	}

	public Square getSquare(int x, int y) {
		return board.getSquare(x, y);
	}

	public boolean setSquare(int x, int y, int token) {
		return board.setSquare(token, x, y, getCurrentPlayer());
	}

	public class Move implements Serializable {
		private int token, x, y;

		public Move(int token, int x, int y) {
			this.token = token;
			this.x = x;
			this.y = y;
		}
	}

	public void shapeDetection() {
		// Detect the Smiler (1)
		boolean smile1 = board.getSquare(1, 1).isFilled();
		boolean smile2 = board.getSquare(3, 1).isFilled();
		boolean smile3 = board.getSquare(0, 3).isFilled();
		boolean smile4 = board.getSquare(1, 4).isFilled();
		boolean smile5 = board.getSquare(2, 4).isFilled();
		boolean smile6 = board.getSquare(3, 4).isFilled();
		boolean smile7 = board.getSquare(4, 3).isFilled();
		if (smile1 && smile2 && smile3 && smile4 && smile5 & smile6 && smile7) {
			processNewAcheivement(1);
		}
		// Detect the Cross (2)
		boolean cross1 = board.getSquare(1, 1).isFilled();
		boolean cross2 = board.getSquare(2, 2).isFilled();
		boolean cross3 = board.getSquare(3, 3).isFilled();
		boolean cross4 = board.getSquare(4, 4).isFilled();
		boolean cross5 = board.getSquare(1, 4).isFilled();
		boolean cross6 = board.getSquare(2, 3).isFilled();
		boolean cross7 = board.getSquare(3, 2).isFilled();
		boolean cross8 = board.getSquare(4, 1).isFilled();
		if (cross1 && cross2 && cross3 && cross4 && cross5 & cross6 && cross7
				&& cross8) {
			processNewAcheivement(2);
		}
		// Detect the circle (3)
		boolean circle1 = board.getSquare(1, 1).isFilled();
		boolean circle2 = board.getSquare(2, 1).isFilled();
		boolean circle3 = board.getSquare(3, 1).isFilled();
		boolean circle4 = board.getSquare(4, 1).isFilled();

		boolean circle5 = board.getSquare(1, 4).isFilled();
		boolean circle6 = board.getSquare(2, 4).isFilled();
		boolean circle7 = board.getSquare(3, 4).isFilled();
		boolean circle8 = board.getSquare(4, 4).isFilled();

		boolean circle9 = board.getSquare(1, 2).isFilled();
		boolean circle10 = board.getSquare(1, 3).isFilled();

		boolean circle11 = board.getSquare(4, 2).isFilled();
		boolean circle12 = board.getSquare(4, 3).isFilled();
		if (circle1 && circle2 && circle3 && circle4 && circle5 && circle6
				&& circle7 && circle8 && circle9 && circle10 && circle1
				&& circle11 && circle12) {
			processNewAcheivement(3);
		}
	}
}

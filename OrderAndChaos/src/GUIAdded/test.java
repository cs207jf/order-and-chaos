package GUIAdded;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class test implements Serializable {
	private static Game session;
	private static Acheivement ac;
	private static boolean AI;
	private static boolean loadGame;
	private static boolean viewAchivements;
	static boolean newGame;
	private static boolean deseralised;

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		/*
		 * Game Setup, Settings taken and used to create game
		 */
		Scanner userInput = new Scanner(System.in);
		newGame = true;
		deseralised = true;
		while (true) {
			if (newGame) {
				System.out.println("Welcome to ORDER & CHAOS");
				ArrayList<Acheivement> oldAcheivements = null;
				deserialize();
				newGame = false;
				if (deseralised) {
					System.out
							.println("Save Game Detected. Would you like to view acheivements yes/no");
					viewAchivements = userInput.nextLine().toLowerCase()
							.contains("yes");
					if (viewAchivements) {
						printAcheivements();
					}
					oldAcheivements = session.acheivements;

					System.out
							.println("Welcome to ORDER & CHAOS\nWould you like to resume a game yes/no");
					loadGame = userInput.nextLine().toLowerCase()
							.contains("yes");
				}
				if (loadGame) {
					printBoard();
				} else {
					System.out
							.println("How would you like to play: single / multi");
					AI = userInput.nextLine().toLowerCase().contains("single");
					boolean AIchaos = false;
					if (AI) {
						System.out
								.println("Who would you like to be: order / chaos");
						AIchaos = userInput.nextLine().toLowerCase()
								.contains("chaos");
					}
					session = new Game(AI, AIchaos);
					if (viewAchivements) {
						session.acheivements = oldAcheivements;
					}
				}
			}

			/*
			 * Game Play starts
			 */

			if (session.getCurrentPlayer().isHuman()) {
				printTurn();
				String input = userInput.nextLine();
				if (input.contains("help")) {
					printHelp();
				} else if (input.contains("turn")) {
					printTurn();
				} else if (input.contains("print")) {
					printBoard();
				} else if (input.contains("achiev")) {
					printAcheivements();
				} else if (input.contains("quit")) {
					System.out.println("Would you like to start a new game?");
					input = userInput.nextLine();
					if (input.contains("no")) {
						System.out.println("Goodbye");
						System.exit(0);
					} else {
						newGame = true;
					}
				} else if (input.contains("undo")) {
					undo();
				} else if (input.contains("save")) {
					System.out.println("game saved.");
					serialize();
				} else {
					makeMove(input);

				}
			} else if (!session.getCurrentPlayer().isHuman()) {
				System.out.println("\n\n######Waiting For Ai...#######\n");
				session.makeAIMove();
				System.out.println("######AI Complete!#######\n");
			}
		}
	}

	private static void printBoard() {
		// TODO Auto-generated method stub
		Controller.refreshBoard();
	}

	private static void printAcheivements() {
		System.out.println("----START ACHEIVEMENTS----");
		for (int i = 0; i < session.acheivements.size(); i++) {
			System.out.println(session.acheivements.get(i).name + " : "
					+ session.acheivements.get(i).description);
		}
		System.out.println("----END ACHEIVEMENTS----");
	}

	public static void quit() {
		Controller.QuitGame();
		//View Stats?	-PrintAcheivments
		//Start New Game? -Loop -Quit
		Scanner userInput = new Scanner(System.in);
		System.out.println("Would you like to view statistics?");
		String input = userInput.nextLine();
		if (input.contains("yes")) {
			printAcheivements();
		}
		System.out.println("Would you like to start a new game?");
		input = userInput.nextLine();
		if (input.contains("no")) {
			System.out.println("Goodbye");
			System.exit(0);
		} else {
			newGame = true;
		}
	}

	private static void undo() {
		Controller.UndoMove();
	}

	/*
	 * Make a Move Options taken and passed to method in game class Board then
	 * reprinted
	 */
	private static void makeMove(String input) {
		if (input.length() == 3) {
			int token = input.charAt(0) == 'x' ? 1 : -1;
			int x = Character.getNumericValue(input.charAt(1));
			int y = Character.getNumericValue(input.charAt(2));
			session.makeMove(token, x, y);
			session.detectAchievements();
			serialize();
		}
	}
	/*
	 * Save selected objects to the file save.ser
	 * 
	 * throws exception if file not found.
	 */
	public static void serialize() {

		try {
			FileOutputStream fs = new FileOutputStream("save.ser");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			os.writeObject(session);
			//os.writeObject(ac);
			os.close();
			fs.close();
		} catch (IOException ex) {

			ex.printStackTrace();
		}

	}

	/*
	 * Opens the file save.ser and reads contents of required objects
	 * 
	 * throws exception if file not found.
	 */
	public static void deserialize() {

		try {
			FileInputStream fs = new FileInputStream("save.ser");
			ObjectInputStream os = new ObjectInputStream(fs);
			Object one = os.readObject();
			Object two = os.readObject();
			session = (Game) one;
			//ac = (Acheivement) two;
			fs.close();
		} catch (IOException i) {
			deseralised = false;
			i.printStackTrace();
		} catch (ClassNotFoundException e) {
			deseralised = false;
			e.printStackTrace();
		}
	}

	private static void printHelp() {
		System.out
				.println("--- Help ---\n@turn - Display's whose turn it is\n@print - displays  the board\n@quit - ends the program");
	}

	private static void printTurn() {
		System.out.println((session.getCurrentPlayer().isOrder() ? "Order"
				: "Chaos") + " its your turn");
	}
}

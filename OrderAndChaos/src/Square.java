public class Square {
	private int x;
	private int y;
	private int value;
	private Square left;
	private Square right;
	private Square up;
	private Square down;
	private Square upLeft;
	private Square upRight;
	private Square downLeft;
	private Square downRight;
	private boolean filled;
	private int player;

	public Square(int x, int y) {
		this.x = x;
		this.y = y;
		filled = false;
	}

	public void initLinks() {
		if (x == 0) {
			setLeft(null);
			setUpLeft(null);
			setDownLeft(null);
		} else {
			setLeft(Board.getSquare(x - 1, y));
			if (y != 5) {
				setUpLeft(Board.getSquare(x - 1, y + 1));
			}
			if (y != 0) {
				setDownLeft(Board.getSquare(x - 1, y - 1));
			}
		}
		if (x == 5) {
			setRight(null);
			setUpRight(null);
			setDownRight(null);
		} else {
			setRight(Board.getSquare(x + 1, y));
			if (y != 5) {
				setUpRight(Board.getSquare(x + 1, y + 1));
			}
			if (y != 0) {
				setDownRight(Board.getSquare(x + 1, y - 1));
			}
		}
		if (y == 0) {
			setDown(null);
			setDownLeft(null);
			setDownRight(null);
		} else {
			setDown(Board.getSquare(x, y - 1));
			if (x != 0) {
				setDownLeft(Board.getSquare(x - 1, y - 1));
			}
			if (x != 5) {
				setDownRight(Board.getSquare(x + 1, y - 1));
			}
		}

		if (y == 5) {
			setUp(null);
			setUpLeft(null);
			setUpRight(null);
		} else {
			setUp(Board.getSquare(x, y + 1));
			if (x != 0) {
				setUpLeft(Board.getSquare(x - 1, y + 1));
			}
			if (x != 5) {
				setUpRight(Board.getSquare(x + 1, y + 1));
			}
		}
	}

	public int getValue() {
		return value;
	}
	
	public boolean setValue(int value){
		if(filled){
			return false;
		}
		else{
			this.value = value;
			return true;
		}
	}

	public boolean setValue(int value, int player) {
		if (filled) {
			return false;
		} else {
			this.value = value;
			setFilled();
			this.player = player;
			return true;
		}
	}

	public Square getLeft() {
		return left;
	}

	public void setLeft(Square left) {
		this.left = left;
	}

	public Square getRight() {
		return right;
	}

	public void setRight(Square right) {
		this.right = right;
	}

	public Square getUp() {
		return up;
	}

	public void setUp(Square up) {
		this.up = up;
	}

	public Square getDown() {
		return down;
	}

	public void setDown(Square down) {
		this.down = down;
	}

	public Square getUpLeft() {
		return upLeft;
	}

	public void setUpLeft(Square upLeft) {
		this.upLeft = upLeft;
	}

	public Square getUpRight() {
		return upRight;
	}

	public void setUpRight(Square upRight) {
		this.upRight = upRight;
	}

	public Square getDownLeft() {
		return downLeft;
	}

	public void setDownLeft(Square downLeft) {
		this.downLeft = downLeft;
	}

	public Square getDownRight() {
		return downRight;
	}

	public void setDownRight(Square downRight) {
		this.downRight = downRight;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled() {
		this.filled = true;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}
}
